import 'package:flutter/material.dart';
//import 'package:ja_peru/src/bloc/provider.dart';
import 'package:ja_peru/src/models/equipo_model.dart';

import 'package:ja_peru/src/providers/equipo_provider.dart';
import 'package:ja_peru/src/theme/theme.dart';
import 'package:ja_peru/src/widgets/menu_widget.dart';
import 'package:provider/provider.dart';
//import 'package:url_launcher/url_launcher.dart';
import 'package:animate_do/animate_do.dart';

class EquipoPage extends StatefulWidget { // lo convertir en Statefull, porque noa ctualizaba las imagenes que subia
  
  @override
  _EquipoPageState createState() => _EquipoPageState();
}

class _EquipoPageState extends State<EquipoPage> {
  final equipoProvider = new EquipoProvider();
  final estiloTitulo    = TextStyle( fontSize: 20.0, fontWeight: FontWeight.bold );
  final estiloSubTitulo = TextStyle( fontSize: 16.0 );
  @override
  Widget build(BuildContext context) {
    final appTheme2 = Provider.of<ThemeChanger>(context).currentTheme;
    //final bloc = Provider.of(context);
    

    return Scaffold(
      appBar: AppBar(
        title: Text('Equipo')
      ),
      drawer: MenuWidget(),
      body: _crearListado(appTheme2),
    
    );
  }

  Widget _crearListado(ThemeData appTheme2) {

    return FutureBuilder(
      future: equipoProvider.cargarEquipo(),
      builder: (BuildContext context, AsyncSnapshot<List<EquipoModel>> snapshot) {
        if ( snapshot.hasData ) {

          final equipo = snapshot.data;

          return ListView.builder(
            
            itemCount: equipo.length,
            itemBuilder: (context,i) => _crearItem(context, equipo[i], i ,appTheme2),
          );

        } else {
          return Center( child: CircularProgressIndicator());
        }
      },
    );
  }

  Widget _crearItem(BuildContext context, EquipoModel equipo, int i,ThemeData appTheme2 ) {

    return SafeArea(
      child: FadeInLeft(
        
              child: Container(
          
          padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 15.0),
          child: Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                      Container(
                      width: 100,
                      height: 100,
                      child: ClipRRect(
                        borderRadius: new BorderRadius.circular(60.0),
                          child: FadeInImage(
                          image: NetworkImage(equipo.foto),
                          placeholder: AssetImage('assets/FEDTpyE.gif'),
                          fit:BoxFit.fill, 
                         ),
                      ),
                    ),
                    SizedBox(width:20,),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('${ equipo.nombre }',style: estiloTitulo ,overflow: TextOverflow.ellipsis, ),
                      SizedBox( height: 5.0 ),
                      Text( equipo.cargo ,overflow: TextOverflow.ellipsis,),
                      SizedBox( height: 5.0 ),
                      Text( equipo.correo,style: TextStyle(fontSize: 15.0, color: Colors.grey), ),
                    ],
                  ),
                ),

              ],
            ),
          ),
        ),
      ),
    );
    


    

  }


}