// To parse this JSON data, do
//
//     final podCastModel = podCastModelFromJson(jsonString);

import 'dart:convert';

PodCastModel podCastModelFromJson(String str) => PodCastModel.fromJson(json.decode(str));

String podCastModelToJson(PodCastModel data) => json.encode(data.toJson());

class PodCastModel {
    PodCastModel({
        this.id,
        this.descripcin,
        this.fotoPod,
        this.linkAudio,
        this.ponente,
        this.posicion,
        this.titulo,
    });

    String id;
    String descripcin;
    String fotoPod;
    String linkAudio;
    String ponente;
    int    posicion;
    String titulo;

    factory PodCastModel.fromJson(Map<String, dynamic> json) => PodCastModel(
        id            : json["id"],
        descripcin    : json["Descripción"],
        fotoPod       : json["FotoPod"],
        linkAudio     : json["LinkAudio"],
        ponente       : json["Ponente"],
        posicion      : json["Posicion"],
        titulo        : json["Titulo"],
    );

    Map<String, dynamic> toJson() => {
        "id"          : id,
        "Descripción" : descripcin,
        "FotoPod"     : fotoPod,
        "LinkAudio"   : linkAudio,
        "Ponente"     : ponente,
        "Posicion"    : posicion,
        "Titulo"      : titulo,
    };
}