// To parse this JSON data, do
//
//     final noticiaModel = noticiaModelFromJson(jsonString);

import 'dart:convert';

NoticiaModel noticiaModelFromJson(String str) => NoticiaModel.fromJson(json.decode(str));

String noticiaModelToJson(NoticiaModel data) => json.encode(data.toJson());

class NoticiaModel {
    NoticiaModel({
        this.id,
        this.link,
        this.post,
        this.programa,
        this.titulo,
    });

    String id;
    String link;
    String post;
    String programa;
    String titulo;

    factory NoticiaModel.fromJson(Map<String, dynamic> json) => NoticiaModel(
        id         : json["id"],
        link       : json["Link"],
        post       : json["Post"],
        programa   : json["Programa"],
        titulo     : json["Titulo"],
    );

    Map<String, dynamic> toJson() => {
        "id"       : id,
        "Link"       : link,
        "Post"     : post,
        "Programa" : programa,
        "Titulo"   : titulo,
    };
}
