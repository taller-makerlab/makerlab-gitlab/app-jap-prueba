// To parse this JSON data, do
//
//     final equipoModel = equipoModelFromJson(jsonString);

import 'dart:convert';

EquipoModel equipoModelFromJson(String str) => EquipoModel.fromJson(json.decode(str));

String equipoModelToJson(EquipoModel data) => json.encode(data.toJson());

class EquipoModel {
    EquipoModel({
        this.id,
        this.cargo,
        this.celular,
        this.correo,
        this.nombre,
        this.foto,
    });

    String id;
    String cargo;
    int celular;
    String correo;
    String nombre;
    String foto;

    factory EquipoModel.fromJson(Map<String, dynamic> json) => EquipoModel(
        id          : json["id"],
        cargo       : json["Cargo"],
        celular     : json["Celular"],
        correo      : json["Correo"],
        nombre      : json["Nombre"],
        foto        : json["foto"],
    );

    Map<String, dynamic> toJson() => {
        "id"        : id,
        "Cargo"     : cargo,
        "Celular"   : celular,
        "Correo"    : correo,
        "Nombre"    : nombre,
        "foto"      : foto,
    };
}
