
import 'dart:convert';


import 'package:http/http.dart' as http;


import 'package:ja_peru/src/models/equipo_model.dart';

class EquipoProvider {

  final String _url = 'https://japeru-4f886-default-rtdb.firebaseio.com/';




  Future<List<EquipoModel>> cargarEquipo() async {

    final url  = '$_url/Equipo.json';
    final resp = await http.get(url);

    final Map<String, dynamic> decodedData = json.decode(resp.body);
    final List<EquipoModel> equipo = new List();


    if ( decodedData == null ) return [];

    decodedData.forEach( ( id, equi ){

      final equipoTemp = EquipoModel.fromJson(equi);
      equipoTemp.id = id;

      equipo.add( equipoTemp );

    });

     //print( equipo[0].id );

    return equipo;

  }



  


}

